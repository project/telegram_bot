Module: Telegram bot
Author: Jennifer Trelewicz <http://drupal.org/user/1601538> <http://tgpo.ru>


Description
===========
Bare-bones integration with the Telegram bot API. We are currently using this
for a cron healthcheck module that reports problems to our Telegram channel

Requirements
============
Variable

Installation
============
Copy the 'telegram_bot' module directory in to your Drupal sites/all/modules directory as usual.

Usage
=====
Configure at admin/config/telegram_bot. This module is intended to be used
by other modules and does not provide a chat interface directly.
